//
//  ServerCallManager.swift
//  PumaStore
//
//  Created by Sanchan on 20/01/17.
//  Copyright © 2017 Sanchan. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

class PSApiManager
{
    static let CallManager:PSApiManager = PSApiManager()
    
    func postParametersWithJson(url:String,parameters:[String:[String:AnyObject]],completion: @escaping(_ responseDict:Any?,_ error:Error?,_ isDone:Bool)-> Void)
    {
        Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.prettyPrinted, headers: nil).responseJSON { response in
            do
            {
                let post:Any = try JSONSerialization.jsonObject(with: response.data!
                    , options: .mutableContainers)
                completion(post, nil, true)
                
            }
            catch
            {
                completion(nil, response.result.error!, false)
            }
            
        }
    }
    
    func getData(url:String,completion: @escaping (_ responseDict:Any?)-> Void)
    {
        Alamofire.request(url).responseJSON{
            response in
            if response.response != nil
            {
                if response.response?.statusCode == 200
                {
                    completion(response.result.value as! NSDictionary)
                }
            }
            else
            {
                completion(nil)
            }
        }
    }
}
