//
//  Constants.swift
//  PumaStore
//
//  Created by Sanchan on 20/01/17.
//  Copyright © 2017 Sanchan. All rights reserved.
//

import Foundation
import UIKit

let kProductUrl = "http://54.158.161.135/gets3Files"
let kAccountInfoUrl = "http://54.158.161.135/getAccountInfo"
let kBaseUrl = "http://s3.amazonaws.com/peafowl/puma-diamond-dev"
let kPayCVV = "http://54.158.161.135/payWithCVV"
var gUserID = String()
var gUserName = String()
var gCustomerId = String()
var gEmailId = String()
let logoImg = "http://s3.amazonaws.com/peafowl/puma-diamond-dev/puma/logo.png"
let kFetchAddress = "http://54.158.161.135/fetchAddressStore"
let focusColor:CGColor = (UIColor.init(red: 200/255, green: 169/255, blue: 133/255, alpha: 1.0)).cgColor
