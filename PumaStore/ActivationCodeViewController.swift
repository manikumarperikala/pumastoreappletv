//
//  CodeViewController.swift
//  PumaStore
//
//  Created by Sanchan on 20/01/17.
//  Copyright © 2017 Sanchan. All rights reserved.
//

import UIKit

class ActivationCodeViewController: UIViewController {

    var uuid,deviceId,userId,code: String!
    
    @IBOutlet weak var activationLbl: UILabel!
    @IBOutlet weak var CodeLbl: UILabel!
    @IBOutlet weak var CodeView: UIView!
    @IBOutlet weak var clkBtn: UIButton!
    var datadict = NSDictionary()
    var timer = Timer()
    var isAction = Bool()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let str = "To activate go to\nhttp://54.158.161.135/"
        let attributedString = NSMutableAttributedString.init(string: str)
        attributedString.addAttribute(NSFontAttributeName, value:self.activationLbl.font, range:NSRange(location: 0, length: str.characters.count))
        attributedString.addAttribute(NSFontAttributeName, value: UIFont.boldSystemFont(ofSize: 32), range: NSRange(location:("To activate go to\n" as String).characters.count,length:("http://54.158.161.135/" as String).characters.count))
        self.activationLbl.attributedText = attributedString
        CodeView.layer.borderWidth = 7.0
        CodeView.layer.borderColor = UIColor.white.cgColor
        CodeLbl.text = code
        timer = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(self.accountstatus), userInfo: nil, repeats: true)
    }
    
    func accountstatus()
    {
        let url = kAccountInfoUrl
        let alertview = UIAlertController(title: "Activation Status", message: "Your Device Activation is Pending" , preferredStyle: .alert)
        let  parameters = [ "getAccountInfo": ["deviceId": deviceId!, "uuid": uuid!]]
        PSApiManager.CallManager.postParametersWithJson(url: url, parameters: parameters as [String : [String : AnyObject]]) {(responseDict , error,isDone) in
            if error == nil
            {
                let post = responseDict
                let dict = post as! NSDictionary
                self.datadict = dict
                let uuidExist = (dict["uuid_exist"] as! Bool)
                if uuidExist == false
                {
                    if self.isAction
                    {
                        let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                        alertview.addAction(defaultAction)
                        self.present(alertview, animated: true, completion: nil)
                        self.isAction = false
                    }
                }
                else if (dict["user_status"] != nil)
                {
                    if (((dict["user_status"]!) as AnyObject).contains("active"))
                    {
                        self.userId = dict["_id"] as! String
                        gUserID = dict["_id"] as! String
                        gCustomerId = dict["customer_id"] as! String
                        gUserName = dict["full_name"] as! String
                        gEmailId = dict["email"] as! String
                        UserDefaults.standard.set(gUserID, forKey: "_id")
                        UserDefaults.standard.set(gCustomerId, forKey: "customer_id")
                        UserDefaults.standard.set(gUserName, forKey: "full_name")
                        UserDefaults.standard.set(gEmailId, forKey: "email")
                        UserDefaults.standard.synchronize()
                        self.timer.invalidate()
                        self.gotoRoot()
                    }
                }
            }
            else
            {
                let alertview = UIAlertController(title: "No Network Detected", message: "check Internet Connection" , preferredStyle: .alert)
                let defaultAction = UIAlertAction(title: "OK", style: .default, handler: {
                    UIAlertAction in
                })
                alertview.addAction(defaultAction)
                self.navigationController?.pushViewController(alertview, animated: true)
            }
        }
    }
    func gotoRoot()
    {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.gotoMenu(userid: userId)
    }


    @IBAction func continueBtn(_ sender: AnyObject) {
        isAction = true
        accountstatus()
    }
}
