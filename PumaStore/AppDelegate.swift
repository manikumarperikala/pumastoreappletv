//
//  AppDelegate.swift
//  PumaStore
//
//  Created by Sanchan on 20/01/17.
//  Copyright © 2017 Sanchan. All rights reserved.
//

import UIKit
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var deviceId,uuid,userId,status: String!
    let activationUrl = "http://54.158.161.135/getActivationCode"
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        let loading = LoaderView.init(frame: (self.window?.frame)!)
        self.window?.addSubview(loading)
        var parameters = [String:[String:AnyObject]]()
        let udid = UIDevice.current.identifierForVendor?.uuidString
        parameters = ["getActivationCode":["model":"AppleTV4Gen" as AnyObject,"manufacturer":"Apple" as AnyObject,"device_name":"AppleTV" as AnyObject, "device_id": udid! as AnyObject ,"device_mac_address":"mac" as AnyObject,"brand_name":"Apple" as AnyObject,"host_name":"app" as AnyObject ,"display_name":"apple" as AnyObject, "serial_number":"1234" as AnyObject ]]
        PSApiManager.CallManager.postParametersWithJson(url: activationUrl, parameters: parameters){
            (responseDict,error,isDone)in
            if error == nil
            {
                let dict = responseDict as! [String:String]
                self.uuid = dict["uuid"]! as String
                self.deviceId = dict["device_id"]! as String
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                if !(dict["pair_devce"]! as String).contains("active")
                {
                    let CodeView = storyBoard.instantiateViewController(withIdentifier: "ActivateCode") as! ActivationCodeViewController
                    CodeView.uuid = self.uuid
                    CodeView.deviceId = self.deviceId
                    CodeView.code = dict["code"]
                    let navigationController = UINavigationController(rootViewController: CodeView)
                    self.window?.rootViewController = navigationController
                    self.window?.makeKeyAndVisible()
                }
                else
                {
                    if (UserDefaults.standard.object(forKey: "_id") != nil)
                    {
                        self.userId = UserDefaults.standard.object(forKey: "_id") as! String
                        self.gotoMenu(userid: self.userId)
                    }
                }
            }
            else
            {
                let alertview = UIAlertController(title: "No Network Detected", message: "check Internet Connection" , preferredStyle: .alert)
                let defaultAction = UIAlertAction(title: "OK", style: .default, handler: {
                    UIAlertAction in
                })
                alertview.addAction(defaultAction)
                self.window?.rootViewController?.present(alertview, animated: true, completion: nil)
            }
        }
        loading.removeFromSuperview()
        return true
    }
    
    func gotoMenu(userid: String)
    {
        PSApiManager.CallManager.getData(url: kProductUrl)
        {
            (responseDict)in
            if responseDict != nil
            {
                let post = responseDict as! NSDictionary
                self.gotoPumaStore(userId:userid,response:post)
            }
            else
            {
                print("json error")
            }
        }
    }
    
    func gotoPumaStore(userId:String,response:NSDictionary)
    {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let rootView = storyBoard.instantiateViewController(withIdentifier: "Store") as! StoreViewController
        rootView.productData = response
        rootView.userId = userId
        let navigationController = UINavigationController(rootViewController: rootView)
        self.window?.rootViewController = navigationController
        self.window?.makeKeyAndVisible()
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
      //  NotificationCenter.default.addObserver(self, selector: #selector(self.myObserverMethod), name:NSNotification.Name.UIApplicationDidEnterBackground, object: nil)

    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ApplicationActivated"), object: nil)
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
      //  self.saveContext()
    }

    // MARK: - Core Data stack

    @available(tvOS 10.0, *)
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "PumaStore")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

  /*  func saveContext () {
        if #available(tvOS 10.0, *) {
            let context = persistentContainer.viewContext
        } else {
            // Fallback on earlier versions
        }
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }*/

}

